// getCube
let num=2
const getCube = num**3;
console.log(`The cube of ${num} is ${getCube}`);

// address
const address = ["258", "Washington Ave. NW", "California", "90011"];
const [number, street, state, postal] = address;
console.log(`I live at ${number} ${street}, ${state} ${postal}`);

// animal
const animal = {
  name: "Lolong",
  type: "saltwater crocodile",
  weight: "1075 kgs",
  length: "20 ft 3 in."
}
const {name, type, weight, length} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}`);

// numbers
const arrayNum = [1,2,3,4,5];
arrayNum.forEach(x=>console.log(x))
const reduceNumber = arrayNum.reduce((a, b)=> a+b);
console.log(reduceNumber);

// constructor
class Dog{
  constructor(name, age, breed){
    this.name=name,
    this.age=age,
    this.breed=breed
  }
};
const dog1 = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog1);