// JS ES6 updates

// exponent operator
  // pre-ES6:
  const firstNum = Math.pow(8, 2);
	console.log(firstNum);

  // es6
  const secondNum = 8**2;
  console.log(secondNum);


  // TEMPLATE LITERALS  
  /* 
  Miniactivity
  create a name variable that accepts and store a name of a person
  create a message, greeting the person and welcoming him/her in the programming field
  log in the console the message
  */

  // es6
  let name = "EJ";
  let message = `Hello ${name}! Welcome to the programming field`;
  console.log(message);

  // Multiple
  const anotherMessage = `${name} attended a math competition. He won it by solving the problem 8**2 with the answer of ${secondNum}`;

  console.log(anotherMessage);

  // computation inside the template
  const interestRate = .10;
  const principal = 1000;
  console.log(`The interest on your savings is ${principal*interestRate}`)


// Array Destructuring - allows unpacking elements in arrays into distinct variables
  /* 
  create an array that has firstName, middleName and LastName elements

  log in the console each element (1 row - 1 element)
  */
 const fullName = ["Juan", "Dela", "Cruz"]
 
  // pre-es6
 fullName.forEach(
  function(name){
    console.log(name)
  }
);

  // es6
  const [firstName, middleName, lastName] = fullName;
  console.log(firstName);
  console.log(middleName);
  console.log(lastName);

/* 
create an object that contains a woman's given name, Maiden Name, Family Name
log in the console each key (1 row - 1key value pair)
*/

let womanName={givenName:"Ellaine", maidenName:"Malelang", familyName: "Cuden"}

console.log(womanName.givenName);
console.log(womanName.maidenName);
console.log(womanName.familyName);

// es6
const {givenName, maidenName, familyName}= womanName;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

// ARROW FUNCTION
/* 
Compact alternative syntax compared to the traditional functions; useful for code snippets where creating functions will be reused in any other parts of the code; Don't Repeat Yourself - there is no need to create a function that will not be used in other parts/portions of the code

*/
/* 
create a function that displays/logs a person's firstName, middleInitial and lastName
*/

// pre-es6
/* 
Parts:
1. declaration
2. functionName
3. Parameters
4. Invoke/call the function
*/
function displayName(){
  console.log("Juan D. Cruz")
};

function displayName(firstName, middleInitial, lastName){
/*   console.log(firstName);
  console.log(middleInitial);
  console.log(lastName); */
  console.log(firstName, middleInitial, lastName)
};

displayName("Juan", "D.", "Cruz");

// es6
const dName=(fname, mname, lname) => {
  console.log(fname, mname, lname);
}
dName("Will", "D.", "Smith");


/* 
  use forEach method to log each student in the console
*/

// es6
  // If you have 2 orn more parameters, eenclose them inside a pair of parenthesis
const students = ["john", "Jane", "Joe"]

students.forEach(x=>console.log(x));

/* 
create a function that adds two numbers using return statement
*/
// pre-es6
function add(x, y){
  return x+y;
};
console.log(add(2,3));
/* 
Implicit Return Statement - return statement/s are omitted because even without them, JS implicitly add them for the resultof the function
*/
// es6
const sum = (x,y) => x+y;
let total=sum(3,4);
console.log(total);

// (Arrow Function) Default Function Arguement Value
/* 
  provides a default argument value if no parameters are included/specified once the function has been invoked
*/
const greet = (name = "User") =>{
  return `Good morning, ${name}!`
};
console.log(greet())
console.log(greet("EJ"));

/* 
create a car object using the object method:
car fields:
name
brand
year
*/

/* let theCar={
  carName: "Mx-5",
  carBrand: "Mazda",
  carYear: 2021
};

function car (name, brand, year){
  this.name=name,
  this.brand=brand,
  this.year=year
}

const car1 = new car("Honda", "Vios", 2020);
console.log(car1); */

// CLASS CONSTRUCTOR
class car{
  // constructor keyword - special method of creating/initializing an object for the "car" class
  constructor (brand, name, year){
    this.name=name,
    this.brand=brand,
    this.year=year
  }
};

const car1 = new car("Mazda", "MX-5", 2021);
console.log(car1)

const car2 = new car();
console.log(car2);
car2.brand="Toyota";
car2.name="Fortuner";
car2.year=2020;
console.log(car2);

/* 
create a variable that stores  a number
creathe an if else statement using ternary operator
condition:
if the number <=0, return true,
false if >0
*/

const num = 10;

if(num<=0){
  console.log(true)
}
else{
  console.log(false)
};

